package com.contactsbook

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Contact(
    @SerializedName("_id") var id: Number = 0,
    @SerializedName("first_name") var firstName: String = "",
    @SerializedName("last_name") var lastName: String = "",
    @SerializedName("sur_name") var surName: String = "",
    @SerializedName("phone") var phone: String = "",
    @SerializedName("email") var email: String = ""
) : Serializable

class DB(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_NAME = "polytech.labacontactsbook.db"
        private const val DATABASE_VERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
            "CREATE TABLE contacts(" +
                    "_id INTEGER PRIMARY KEY," +
                    "first_name TEXT," +
                    "last_name TEXT," +
                    "sur_name TEXT," +
                    "phone TEXT," +
                    "email TEXT" +
                    ")"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS TABLE_contacts")
        onCreate(db)
    }

    fun addContact(contact: Contact): Int {
        val values = ContentValues()
        values.put("first_name", contact.firstName)
        values.put("last_name", contact.lastName)
        values.put("sur_name", contact.surName)
        values.put("phone", contact.phone)
        values.put("email", contact.email)

        val db = this.writableDatabase

        val id = db.insert("contacts", null, values)
        Log.d("DB", id.toString())
        db.close()
        return id.toInt()
    }

    fun editContact(contact: Contact): Contact {
        val values = ContentValues()
        values.put("first_name", contact.firstName)
        values.put("last_name", contact.lastName)
        values.put("sur_name", contact.surName)
        values.put("phone", contact.phone)
        values.put("email", contact.email)

        val db = this.writableDatabase

        db.update("contacts", values, "_id = ?", arrayOf(contact.id.toString()))

        db.close()
        return contact
    }

    fun deleteContact(contact: Contact): Contact {
        val db = this.writableDatabase;

        db.delete("contacts", "_id = ?", arrayOf(contact.id.toString()))

        db.close()
        return contact
    }

    fun getContacts(): ArrayList<Contact> {
        val contacts = ArrayList<Contact>()
        val db = this.writableDatabase;
        val cursor = db.rawQuery("SELECT * FROM contacts", null)

        if (cursor.moveToFirst()) {
            cursor.moveToFirst()

            do {
                val contact = Contact()
                contact.id = cursor.getInt(0)
                contact.firstName = cursor.getString(1)
                contact.lastName = cursor.getString(2)
                contact.surName = cursor.getString(3)
                contact.email = cursor.getString(4)
                contact.phone = cursor.getString(5)
                contacts.add(contact)
            } while (cursor.moveToNext())

            cursor.close()
        }

        db.close()

        return contacts
    }
}